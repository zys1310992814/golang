package gogame

import (
    "container/list"
)

type State struct {
    Clients *list.List
}

type StateUpdateFunc func(s *State)

func (s *State) UpdateLoop(ch chan StateUpdateFunc) {
    for f := range ch {
        f(s)
    }
}
