package gogame

import (
    "net"
    "container/list"
)

func StartServer(addr string, st chan StateUpdateFunc) {
    Log("Starting server.")
    socket, err := net.Listen("tcp", addr)
    if err != nil {
        Log("Could not spawn server.")
    }
    for {
        Log("listening...")
        conn, err := socket.Accept()
        if err != nil {
            Log("Failed to accept a connection.")
            continue
        }
        go NewClient(conn, st)
    }
}

func Main() {
    state := State{list.New()}
    stateUpdate := make(chan StateUpdateFunc, 4096)
    go state.UpdateLoop(stateUpdate)
    StartServer(":8080", stateUpdate)
}