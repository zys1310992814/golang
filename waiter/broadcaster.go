package main

import (
	"log"
	"time"

	"github.com/sauerbraten/waiter/enet"
)

type PacketToBroadcast struct {
	Sender ClientNumber
	Packet Packet
}

// Basically a queue, but more complex for stuff sent on channel 0 since position packets are atomic (i.e. a position packet is overwritten by a new one coming in).
type BroadcastBuffer interface {
	add(Packet)
	clear()
	toPacket() Packet
}

type Channel0BroadcastBuffer struct {
	position     Packet // N_POS packets
	otherPackets Packet // N_JUMPPAD, N_TELEPORT, ...
}

func (c0bb *Channel0BroadcastBuffer) add(p Packet) {
	if NetworkMessageCode(p.buf[0]) == N_POS {
		// replace position
		c0bb.position = p
	} else {
		// append other packets (for example N_JUMPPAD)
		c0bb.otherPackets.putBytes(p.buf)
	}
}

// Clears only non-position stuff (there should always be a position packet as long as the client is connected).
func (c0bb *Channel0BroadcastBuffer) clear() {
	c0bb.otherPackets.clear()
}

// Removes both, position and non-position packets. Use this when resetting a client instance.
func (c0bb *Channel0BroadcastBuffer) reset() {
	c0bb.clear()
	c0bb.position.clear()
}

func (c0bb *Channel0BroadcastBuffer) toPacket() Packet {
	return NewPacket(c0bb.position, c0bb.otherPackets)
}

type Channel1BroadcastBuffer struct {
	packets Packet
	cn      ClientNumber // needed for the prefix in toPacket()
}

func (c1bb *Channel1BroadcastBuffer) add(p Packet) {
	c1bb.packets.putBytes(p.buf)
}

func (c1bb *Channel1BroadcastBuffer) clear() {
	c1bb.packets.clear()
}

func (c1bb *Channel1BroadcastBuffer) reset() {
	c1bb.clear()
}

func (c1bb *Channel1BroadcastBuffer) toPacket() Packet {
	if c1bb.packets.len() == 0 {
		return Packet{}
	}
	return NewPacket(N_CLIENT, c1bb.cn, c1bb.packets.len(), c1bb.packets)
}

type Broadcaster struct {
	packetsToBroadcast       <-chan PacketToBroadcast
	flags                    enet.PacketFlag
	channel                  uint8
	ticker                   *time.Ticker
	getClientBroadcastBuffer func(client *Client) BroadcastBuffer
	combinedPacket           Packet
}

func StartBroadcaster(interval time.Duration, flags enet.PacketFlag, channel uint8, getClientBroadcastBuffer func(client *Client) BroadcastBuffer) chan<- PacketToBroadcast {
	ptb := make(chan PacketToBroadcast)

	b := Broadcaster{
		packetsToBroadcast: (<-chan PacketToBroadcast)(ptb),
		flags:              flags,
		channel:            channel,
		ticker:             time.NewTicker(interval),
		getClientBroadcastBuffer: getClientBroadcastBuffer,
		combinedPacket:           Packet{},
	}

	go b.run()

	return chan<- PacketToBroadcast(ptb)
}

func (b *Broadcaster) run() {
	for {
		select {
		case <-b.ticker.C:
			b.flush()

		case ptb := <-b.packetsToBroadcast:
			client := clients[ptb.Sender]
			if !client.Joined {
				continue
			}

			b.getClientBroadcastBuffer(client).add(ptb.Packet)
		}
	}
}

func (b *Broadcaster) flush() {
	for _, client := range clients {
		if !client.Joined {
			continue
		}

		p := b.getClientBroadcastBuffer(client).toPacket()

		if p.len() == 0 {
			continue
		}

		b.combinedPacket.putBytes(p.buf)
	}

	if b.combinedPacket.len() == 0 {
		return
	}

	// double packet
	b.combinedPacket.putBytes(b.combinedPacket.buf)

	if b.channel == 1 {
		log.Println(b.combinedPacket.buf)
	}

	for _, client := range clients {
		if !client.Joined {
			continue
		}

		p := b.getClientBroadcastBuffer(client).toPacket()
		b.getClientBroadcastBuffer(client).clear()

		length := p.len()
		b.combinedPacket.pos += length

		if b.channel == 1 {
			log.Println(p.buf)
		}

		if b.combinedPacket.len() == length*2 {
			// only the client's own packages are in the master packet
			continue
		}

		client.send(b.flags, b.channel, NewPacket(b.combinedPacket.buf[b.combinedPacket.pos:b.combinedPacket.pos+(b.combinedPacket.len()/2)-length]))
	}

	mustFlush = true
	b.combinedPacket.clear()
}
