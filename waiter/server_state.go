package main

import (
	"time"

	"github.com/sauerbraten/waiter/enet"
)

type ServerState struct {
	MasterMode  MasterMode
	GameMode    GameMode
	Map         string
	TimeLeft    int32 // in milliseconds
	NotGotItems bool
	HasMaster   bool // true if one or more clients have master privilege or higher
	UpSince     time.Time
}

func (state *ServerState) changeMap(mapName string) {
	state.NotGotItems = true
	state.Map = mapName
	clients.send(enet.PACKET_FLAG_RELIABLE, 1, NewPacket(N_MAPCHANGE, state.Map, state.GameMode, state.NotGotItems))
	clients.send(enet.PACKET_FLAG_RELIABLE, 1, NewPacket(N_TIMELEFT, state.TimeLeft/1000))
	for _, c := range clients {
		if !c.InUse || c.GameState.State == CS_SPECTATOR {
			continue
		}
		c.GameState.reset()
		c.sendSpawnState()
	}
}
