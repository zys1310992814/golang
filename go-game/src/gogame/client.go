package gogame

import (
    "bufio"
    "net"
)

type Message string

type Client struct {
    Connection net.Conn
    Outgoing chan Message
    Quit chan bool
}

// Create a new client and send it on the channel
// Messages come in through the Incoming channel.
// Send messages through the Outgoing channel.
func NewClient(connection net.Conn, st chan StateUpdateFunc) {
    out := make(chan Message)
    quit := make(chan bool)
    client := &Client{connection, out, quit}
    st <- func (s *State) { AddClient(s, client) }
    Log("new")
    defer client.Close()
    go client.ReadLoop(st)
    client.SendLoop(out)
}

func AddClient(s *State, c *Client) {
    s.Clients.PushBack(c)
    Log("Client connected.")
}

func (c *Client) ReadLoop(st chan StateUpdateFunc) {
    reader := bufio.NewReader(c.Connection)
    for {
        line, err := reader.ReadString('\n')
        if err != nil {
            break
        }
        ParseMessage(line, st)
    }
}

func ParseMessage(line string, st chan StateUpdateFunc) {
    f := func (s *State) {
        Log("Updating state: ", line)
    }
    st <- f
}

func (c *Client) SendLoop(out chan Message) {
    writer := bufio.NewWriter(c.Connection)
    for toSend := range out {
        _, err := writer.WriteString(string(toSend))
        if err != nil {
            return
        }
    }
}

func (c *Client) Close() {
    c.Quit <- true
    c.Connection.Close()
}

func (c *Client) Send(msg Message) {
    c.Outgoing <- msg
}
